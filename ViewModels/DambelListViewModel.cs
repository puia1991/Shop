﻿using Shop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.ViewModels
{
    public class DambelListViewModel
    {
        public IEnumerable<Dambel> Dambels { get; set; }
        public String CurrentCategory { get; set; }
    }
}
