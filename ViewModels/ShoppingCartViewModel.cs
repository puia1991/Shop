﻿using Shop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.ViewModels
{
    public class ShoppingCartViewModel
    {


        // in view model i can't use model 
        public ShoppingCart shoppingCart { get; set; }
        public decimal  ShoppingCartTotal { get; set; }
    }
}
