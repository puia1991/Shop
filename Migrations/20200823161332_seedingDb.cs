﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.Migrations
{
    public partial class seedingDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "categories",
                columns: new[] { "CategoryId", "CategoryDescription", "CategoryName" },
                values: new object[] { 1, null, "gym dambel" });

            migrationBuilder.InsertData(
                table: "categories",
                columns: new[] { "CategoryId", "CategoryDescription", "CategoryName" },
                values: new object[] { 2, null, "house dambel" });

            migrationBuilder.InsertData(
                table: "categories",
                columns: new[] { "CategoryId", "CategoryDescription", "CategoryName" },
                values: new object[] { 3, null, "park dambel" });

            migrationBuilder.InsertData(
                table: "Dambels",
                columns: new[] { "DambelId", "CategoryId", "Desciption", "ImageThumbnailUrl", "ImageURL", "IsOnSale", "IsOnStock", "Name", "Price" },
                values: new object[] { 1, 1, "good for use in the associated environment", "~\\Images\\dambel.png", "~\\Images\\dambel.png", true, false, "gym dambel 12kg", 6.50m });

            migrationBuilder.InsertData(
                table: "Dambels",
                columns: new[] { "DambelId", "CategoryId", "Desciption", "ImageThumbnailUrl", "ImageURL", "IsOnSale", "IsOnStock", "Name", "Price" },
                values: new object[] { 2, 2, "good for use in the associated environment", "~\\Images\\dambel2.jpg", "~\\Images\\dambel2.jpg", true, true, "house dambel 2.5kg", 2.50m });

            migrationBuilder.InsertData(
                table: "Dambels",
                columns: new[] { "DambelId", "CategoryId", "Desciption", "ImageThumbnailUrl", "ImageURL", "IsOnSale", "IsOnStock", "Name", "Price" },
                values: new object[] { 3, 3, "good for use in the associated environment", "~\\Images\\dambel3.jpg", "~\\Images\\dambel3.jpg", false, true, "Park dambel 5kg", 4m });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Dambels",
                keyColumn: "DambelId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Dambels",
                keyColumn: "DambelId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Dambels",
                keyColumn: "DambelId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "categories",
                keyColumn: "CategoryId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "categories",
                keyColumn: "CategoryId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "categories",
                keyColumn: "CategoryId",
                keyValue: 3);
        }
    }
}
