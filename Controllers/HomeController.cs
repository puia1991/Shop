﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Shop.Models;
using Shop.ViewModels;

namespace Shop.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDambelRepositoy _dambelRepositoy;
        public HomeController(IDambelRepositoy dambelRepositoy)
        {
            _dambelRepositoy = dambelRepositoy;
        }

        public IActionResult Index()
        {
            var homeViewModel = new HomeViewModel();
            homeViewModel.DambelssOnSale = _dambelRepositoy.GetDambelsOnSale;
            
            return View(homeViewModel);
            
        }
    }
}
