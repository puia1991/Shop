﻿using Microsoft.AspNetCore.Mvc;
using Shop.Models;
using Shop.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Controllers
{
    public class DambelController : Controller 
    {
        private readonly IDambelRepositoy _dambelRepositoy;
        private readonly ICategoryRepositorycs _categoryRepositorycs;

        public DambelController(IDambelRepositoy dambelRepositoy, ICategoryRepositorycs categoryRepositorycs)
        {
            _dambelRepositoy = dambelRepositoy;
            _categoryRepositorycs = categoryRepositorycs;
        }

        public IActionResult List()
        {
            //ViewBag.CurrentCategory = "Best Sellers";
            //return View(_dambelRepositoy.GetAllDambels);
            var DambelListViewModel = new DambelListViewModel();
            DambelListViewModel.Dambels = _dambelRepositoy.GetAllDambels;
            DambelListViewModel.CurrentCategory = "Best sellers";
            return View(DambelListViewModel);

        }

        public IActionResult Details(int Id)
        {
            var Dambel = _dambelRepositoy.GetDambel(Id);
            if (Dambel == null)
                return NotFound();
            return View(Dambel);
        }
        
    }
}
