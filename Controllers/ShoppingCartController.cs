﻿using Microsoft.AspNetCore.Mvc;
using Shop.Models;
using Shop.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Shop.Controllers
{
    public class ShoppingCartController : Controller
    {

        private readonly IDambelRepositoy _dambelRepositoy;
        private readonly ShoppingCart _shoppingCart;
        public ShoppingCartController(IDambelRepositoy dambelRepositoy , ShoppingCart shoppingCart)
        {
            _dambelRepositoy = dambelRepositoy;
            _shoppingCart = shoppingCart;
        } 

        public ViewResult Index()
        {
            // this part of code is  weird 
            _shoppingCart.ShoppingCartItem = _shoppingCart.GetShoppingCartItems();
            var shoppingCartViewModel = new ShoppingCartViewModel
            {
                shoppingCart = _shoppingCart,
                ShoppingCartTotal = _shoppingCart.GetShoppingCartTotal()

            };
            return View(shoppingCartViewModel);
           
        }

        // this action just adds a item to the shopping cart and return the same view ( the index view but updated )
        public RedirectToActionResult AddToShoppingCart(int dambelId)

        // why can i use Where instead of FirstOrDefaul in this line of code ?
        {
            var selectedDambel = _dambelRepositoy.GetAllDambels.FirstOrDefault(s => s.DambelId == dambelId);
            if (selectedDambel != null)
            {
                _shoppingCart.AddToCart(selectedDambel, 1);
            }

            return RedirectToAction("Index");
        }


        public RedirectToActionResult RemoveFromShoppingCart(int dambelId)
        {
            var selectedDambel = _dambelRepositoy.GetAllDambels.FirstOrDefault(s => s.DambelId == dambelId);
            if ( selectedDambel != null)
            {
                _shoppingCart.RemoveFromCart(selectedDambel);
            }
            return RedirectToAction("Index");
        }


    }
}