﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class ShoppingCart
    {
        
        // property of the shhopping cart 
        
        public string ShoppingCartId { get; set; }
        public List<ShoppingCartItem> ShoppingCartItem { get; set; }

        // in order to access the daba base information 
        private readonly AppDbContext _appDbContext;
        public ShoppingCart(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;       
        }

        public static ShoppingCart GetCard(IServiceProvider services)
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?.HttpContext.Session;
            var context = services.GetService<AppDbContext>();
            string cartId = session.GetString("CartId") ?? Guid.NewGuid().ToString(); // if the CartId doesnot exist , so create a new session ID 
            // so we have out session ready, we have created a new one or grabed the one already existed 
            session.SetString("CartId", cartId);
            return new ShoppingCart(context) { ShoppingCartId = cartId };
        }

        public void AddToCart(Dambel dambel, int amount)
        {
            // to check if the Dambel we  are agoing to add to the cart exist  & chek if the shopping cart id exists as well 
            var ShoppingCartItem = _appDbContext.ShoppingCartItems.FirstOrDefault(s => s.dambel.DambelId == dambel.DambelId && s.ShoppingcartId == ShoppingCartId);

            if (ShoppingCartItem == null)
            {
                ShoppingCartItem = new ShoppingCartItem
                {
                    ShoppingcartId = ShoppingCartId,
                    dambel = dambel,
                    Amount = amount
                };

                _appDbContext.ShoppingCartItems.Add(ShoppingCartItem);
            }
            else
            {
                ShoppingCartItem.Amount++;
            }
            _appDbContext.SaveChanges();
        }

        public int RemoveFromCart(Dambel dambel)
        {
            var ShoppingCartitem = _appDbContext.ShoppingCartItems.FirstOrDefault(s => s.dambel.DambelId == dambel.DambelId && s.ShoppingcartId == ShoppingCartId);

            var localAmount = 0;
            if (ShoppingCartitem != null)
            {
                if (ShoppingCartitem.Amount > 1)
                {
                    ShoppingCartitem.Amount--;
                    localAmount = ShoppingCartitem.Amount;


                }
                else
                {
                    _appDbContext.ShoppingCartItems.Remove(ShoppingCartitem);
                }
            }
            _appDbContext.SaveChanges();
            return localAmount;

        }


        public List<ShoppingCartItem> GetShoppingCartItems()
        {

            return ShoppingCartItem ?? _appDbContext.ShoppingCartItems.Where(c => c.ShoppingcartId == ShoppingCartId).Include(d => d.dambel).ToList();

            // Antonio has written these stuff 
            //var shoppingId = _appDbContext.ShoppingCard.Include(s => s.ShoppingCartItem).Select(x => x.ShoppingCartId).FirstOrDefault();
            //var shoppingCartItems = _appDbContext.ShoppingCartItem.Include(x => x.dambel).Where(s => s.ShoppingcartId == shoppingId).ToList();

            //return shoppingCartItems;



        }


        public void ClearCart()
        {
            var cartItems = _appDbContext.ShoppingCartItems.Where(s => s.ShoppingcartId == ShoppingCartId);
            _appDbContext.ShoppingCartItems.RemoveRange(cartItems);
            _appDbContext.SaveChanges();
        }


        public decimal GetShoppingCartTotal()
        {
            var total = _appDbContext.ShoppingCartItems.Where(s => s.ShoppingcartId == ShoppingCartId).Sum(c => c.dambel.Price * c.Amount);
            return total;
        }


    }
}
