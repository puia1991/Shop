﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class DambelRepository : IDambelRepositoy
    {

        private readonly AppDbContext _appDbContext;
        public DambelRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }
        
        public IEnumerable<Dambel> GetAllDambels 
        {
            get
            {
                return _appDbContext.Dambels.Include(c => c.Category);
            }
        }

        public IEnumerable<Dambel> GetDambelsOnSale
        {
            get
            {
                return _appDbContext.Dambels.Include(c => c.Category).Where(p => p.IsOnSale);
            }
        }

        public Dambel GetDambel(int dambelId)
        {
            return _appDbContext.Dambels.FirstOrDefault(c => c.DambelId == dambelId);
        }

       
    }
}
