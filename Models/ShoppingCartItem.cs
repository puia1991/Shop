﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class ShoppingCartItem
    {
        
        public int ShoppingCartItemId { get; set; }
        public string ShoppingcartId { get; set; }
        public Dambel  dambel { get; set; }
        public  int Amount { get; set; }
    }
}
