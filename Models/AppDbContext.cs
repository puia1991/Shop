﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Shop.Models
{
    public class AppDbContext : DbContext
    {
       
        public AppDbContext(DbContextOptions<AppDbContext> options) :
            base(options) 
        {
        }

        public DbSet<Dambel> Dambels { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<ShoppingCartItem> ShoppingCartItems { get; set; }
        
        public DbSet<ShoppingCart> ShoppingCards { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Category>().HasData(new Category { CategoryId = 1, CategoryName = "gym dambel" });
            modelBuilder.Entity<Category>().HasData(new Category { CategoryId = 2, CategoryName = "house dambel" });
            modelBuilder.Entity<Category>().HasData(new Category { CategoryId = 3, CategoryName = "park dambel" });
           

            modelBuilder.Entity<Dambel>().HasData(new Dambel
            {
                DambelId = 1,
                Name = "gym dambel 12kg",
                Desciption = "good for use in the associated environment",
                ImageURL = @"~\Images\dambel.png",
                ImageThumbnailUrl = @"~\Images\dambel.png",
                Price = 6.50M,
                IsOnSale = true,
                IsOnStock=false,
                CategoryId=1
                
            }) ;
            modelBuilder.Entity<Dambel>().HasData(new Dambel
            {
                DambelId = 2,
                Name = "house dambel 2.5kg",
                Desciption = "good for use in the associated environment",
                ImageURL = @"~\Images\dambel2.jpg",
                ImageThumbnailUrl = @"~\Images\dambel2.jpg",
                Price = 2.50M,
                IsOnSale = true,
                IsOnStock = true,
                CategoryId = 2

            });
            modelBuilder.Entity<Dambel>().HasData(new Dambel
            {
                DambelId = 3,
                Name = "Park dambel 5kg",
                Desciption = "good for use in the associated environment",
                ImageURL = @"~\Images\dambel3.jpg",
                ImageThumbnailUrl = @"~\Images\dambel3.jpg",
                Price = 4M,
                IsOnSale = false,
                IsOnStock = true,
                CategoryId = 3

            });
        }

    }
}
