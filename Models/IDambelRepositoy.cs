﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public interface IDambelRepositoy
    {
        IEnumerable<Dambel> GetAllDambels { get; }
        IEnumerable<Dambel> GetDambelsOnSale { get; } //  this property coonsist of a get function 
        Dambel GetDambel(int Id);

    }
}
