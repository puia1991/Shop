﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class Dambel
    {
        public int DambelId { get; set; }
        public string Name { get; set; }
        public string Desciption { get; set; }

        [Column(TypeName = "decimal(18,4)")]
        public decimal Price { get; set; }
        public string ImageURL { get; set; }
        public string ImageThumbnailUrl { get; set; }
        public bool IsOnSale { get; set; }
        public bool IsOnStock { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }



    }
}
